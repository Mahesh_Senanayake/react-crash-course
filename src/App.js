import Todo from "./components/Todo";

function App() {
  return (
    <div>
      <h1>My ToDos</h1>
      <Todo text='Learn React' />
      <Todo text='Master react' />
      <Todo text='Explore the React course' />
    </div>
  );
}

export default App;
